#!/usr/bin/python3

import json
import os
import sys

SPECFILE = "%define product_name Endpoint service for ___app___\n%define vendor_name ___app___.com\n%define vendor_website https://www.___app___.com/\n%define company_name INGRAM MICRO EUROPEAN SERVICES SL\n%define rpm_prefix ___app___\n%define distribution_suffix el7\n%define version 2.2\n%define www_http_dir /var/www/html\n%define ___app____dir %{www_http_dir}/%{rpm_prefix}\n%define ___app____scripts_dir %{___app____dir}\n%define ___app____dir_log /var/log/aps\n%define aps_port 443\n%define release %(echo ${BUILD_NUMBER:-unknown})\n\nName: %{rpm_prefix}\nVersion: %{version}\nRelease: %{release}.%{distribution_suffix}\nSummary: %{product_name}\nSource0: %{rpm_prefix}-%{distribution_suffix}.tar.gz\nLicense: %{company_name}\nGroup: Applications/Internet\nBuildRoot: %{tmp_build_dir}\nExclusiveArch: x86_64\nVendor: %{vendor_name}\nUrl: %{vendor_website}\n\nRequires: httpd >= 2.4.6\nRequires: mod_ssl >= 2.4.6\nRequires: rsyslog >= 7.4.7\nRequires: logrotate >= 3.8.6-7\nRequires: php >= 5.4.16\nRequires: php-intl >= 5.4.16\nRequires: php-soap >= 5.4.16\nRequires: php-xml >= 5.4.16\n\n%description\n___app___ lets you schedule backups to run daily, weekly, or monthly—it’s your choice. Our award-winning cloud backup\nservice offers automatic backup protection with the option to schedule your backups continuously throughout the day.\n___app___ is affordable backup that saves you time and money (and headaches) so you can focus on other important things,\nlike growing your business.\n\n%prep\n%setup -n %{name} -q\n\n%build\n\n%install\ntar -xvvzf %{rpm_prefix}-%{distribution_suffix}.tar.gz -C $RPM_BUILD_ROOT/\n\n%files\n%defattr(0640,apache,apache,0550)\n%attr(0644, apache, apache) /etc/rsyslog.d/%{rpm_prefix}.conf\n%attr(0644, root, root) /etc/logrotate.d/___app___\n%attr(0644, root, root) /etc/php.d/ioncube____app___.ini\n%attr(0644, root, root) /etc/httpd/conf.d/___app___.conf\n%dir %attr(0555, root, root) %{___app____dir}\n%{___app____scripts_dir}/*.php\n%{___app____scripts_dir}/*.json\n%{___app____scripts_dir}/*.lock\n\n%dir %attr(0770, apache, apache) %{___app____scripts_dir}/config\n%attr(0770, apache, apache) %{___app____scripts_dir}/config/.htaccess\n%dir %attr(0770, apache, apache) %{___app____scripts_dir}/typeCache\n%{___app____scripts_dir}/typeCache/.readme\n\n%dir %{___app____scripts_dir}/schemas\n%{___app____scripts_dir}/schemas/*.schema.gen\n%{___app____scripts_dir}/schemas/.htaccess\n%{___app____scripts_dir}/schemas/APP-META.xml\n\n%dir %{___app____scripts_dir}/configuration\n%{___app____scripts_dir}/configuration/*.json\n\n%dir %{___app____scripts_dir}/exceptions\n%{___app____scripts_dir}/exceptions/*.php\n\n%dir %{___app____scripts_dir}/lib\n%dir %{___app____scripts_dir}/lib/loaders\n%{___app____scripts_dir}/lib/loaders/*.so\n%dir %{___app____scripts_dir}/lib/SoapStructures\n%{___app____scripts_dir}/lib/SoapStructures/*.php\n%{___app____scripts_dir}/lib/*.php\n%dir %{___app____scripts_dir}/lib/step/___app___ResourceName\n%{___app____scripts_dir}/lib/step/___app___ProAccount/*.php\n%dir %{___app____scripts_dir}/lib/step/___app___ProAccountGroup\n%{___app____scripts_dir}/lib/step/___app___ProAccountGroup/*.php\n%dir %{___app____scripts_dir}/lib/step/___app___ProAccountUser\n%{___app____scripts_dir}/lib/step/___app___ProAccountUser/*.php\n%dir %{___app____scripts_dir}/lib/step/___app___ProAccountUserLicense\n%{___app____scripts_dir}/lib/step/___app___ProAccountUserLicense/*.php\n\n%dir %{___app____scripts_dir}/vendor\n%{___app____scripts_dir}/vendor/*.php\n\n%dir %{___app____scripts_dir}/vendor/composer\n%{___app____scripts_dir}/vendor/composer/*.php\n%{___app____scripts_dir}/vendor/composer/*.json\n%{___app____scripts_dir}/vendor/composer/LICENSE\n\n%dir %{___app____scripts_dir}/vendor/psr/log/Psr\n%{___app____scripts_dir}/vendor/psr/log/LICENSE\n%{___app____scripts_dir}/vendor/psr/log/Psr/*\n\n%dir %{___app____scripts_dir}/vendor/ingram-micro-cloud\n%dir %{___app____scripts_dir}/vendor/ingram-micro-cloud/logger\n%{___app____scripts_dir}/vendor/ingram-micro-cloud/logger/*\n%dir %{___app____scripts_dir}/vendor/ingram-micro-cloud/aps-step\n%dir %{___app____scripts_dir}/vendor/ingram-micro-cloud/aps-step/src\n%{___app____scripts_dir}/vendor/ingram-micro-cloud/aps-step/src/*.php\n\n%dir %{___app____scripts_dir}/lib/runtime\n%dir %{___app____scripts_dir}/lib/runtime/aps\n%dir %{___app____scripts_dir}/lib/runtime/aps/2\n%dir %{___app____scripts_dir}/lib/runtime/aps/2/formats\n%{___app____scripts_dir}/lib/runtime/aps/2/*.php\n%{___app____scripts_dir}/lib/runtime/aps/2/formats/*.php\n%{___app____scripts_dir}/lib/runtime/aps/2/types\n\n%clean\nrm -rf $RPM_BUILD_ROOT\n\n%post\numask 077\n\nsed -ie '/<Directory\s\{1,\}\"\/var\/www\/html\/___app___\">/,/Directory/d' /etc/httpd/conf/httpd.conf\nHTTPD_VERSION=$(rpm -q --qf \"%{VERSION}\" httpd| cut -c3)\n\n/usr/sbin/usermod -a -G apache apache\n/bin/systemctl restart httpd.service\n\nAPS_FOLDER=%{rpm_prefix}\necho -e \"\tAPS Endpoint Address: https://$HOSTNAME:%{aps_port}/%{rpm_prefix}\"\nif [[ -x /sbin/ifconfig ]]\n    then\n    echo -e \"\tOther possible options:\"\n    for i in $(/sbin/ifconfig | awk '/inet /&&!/127.0.0.1/ {if (match($2, \":\")) print substr($2, 6); else print $2}')\n    do\n        echo -e \"\t* https://$i:%{aps_port}\"\n    done\nfi\n\n#####\nif [[ ! -d /var/log/aps ]]\nthen\n    mkdir -p /var/log/aps\nfi\n/bin/systemctl restart rsyslog.service\n\n\n%postun\n/bin/systemctl restart httpd.service\n/bin/systemctl restart rsyslog.service"
DOCKERINIFILE = "[DOCKER]\nos = \"centos\"\nosversion = 7\nports[] = 80\nports[] = 443\nrun[] = \"yum install -y epel-release\"\nrun[] = \"yum install -y install httpd php php-soap php-intl php-pecl-oauth php-xml php-xmlrpc php-pdo php-mysql php-pgsql php-curl\"\nrun[] = \"yum install -y mod_ssl\"\nrun[] = \"yum install -y perl perl-libs\"\nrun[] = \"yum install -y http://storage.dev.cloud.im:81/apache-helper/federated-httpd-helper-1.0-9.ce7.noarch.rpm\"\nrundisabled = \"yum update\"\nrpmPrefix = \"___app___\"\nrpmArch = \"x86_64\""
DOCKERPHPFILE = "<?php\n\ndefine(\"DS\", DIRECTORY_SEPARATOR);\ndefine(\"APS_FOLDER\", __DIR__ . DS . \"..\" . DS);\ndefine(\"APP_META_FILE\", APS_FOLDER . \"APP-META.xml\");\ndefine(\"DOCKER_INI_FILE\", __DIR__ . DS . \"docker.ini\");\ndefine(\"INGRAM_DEV_CATALOG_STORAGE\", \"http://10.4.32.122:8080/storage/\");\ndefine(\"DOCKER_FILE\", __DIR__ . DS . \"Dockerfile\");\ndefine(\"DEPLOYMENT_XML_FILE\", __DIR__ . DS . \"..\" . DS . \"deployment.xml\");\n\n$appmeta = simplexml_load_file(APP_META_FILE);\n$inifile = parse_ini_file(DOCKER_INI_FILE);\n\ndefine(\"BUILD_NUMBER\", getenv(\"BUILD_NUMBER\"));\nif (!defined('BUILD_NUMBER')) {\n    die(\"NO BUILD_NUMBER. EXIT.\");\n}\ndefine(\"VENDOR_NAME\", rawurlencode($appmeta->vendor->name));\nif (!defined('VENDOR_NAME')) {\n    die(\"NO VENDOR_NAME. EXIT.\");\n}\ndefine(\"APS_NAME\", rawurlencode($appmeta->name));\nif (!defined('APS_NAME')) {\n    die(\"NO APS_NAME. EXIT.\");\n}\ndefine(\"APS_VERSION\", $appmeta->version);\nif (!defined('APS_VERSION')) {\n    die(\"NO APS_VERSION. EXIT.\");\n}\ndefine(\"FULL_RELEASE_VERSION\", APS_VERSION . \"-\" . BUILD_NUMBER);\nif (!defined('FULL_RELEASE_VERSION')) {\n    die(\"NO FULL_RELEASE_VERSION. EXIT.\");\n}\ndefine(\"PACKAGER_NAME\", rawurlencode($appmeta->packager->name));\nif (!defined('PACKAGER_NAME')) {\n    die(\"NO PACKAGER_NAME. EXIT.\");\n}\ndefine(\"RPM_ARCH\", $inifile['rpmArch']);\nif (!defined('RPM_ARCH')) {\n    die(\"NO RPM_ARCH. EXIT.\");\n}\ndefine(\"RPM_DISTRO\", \"el7\");\ndefine(\"RPM_NAME\", \"___app___-\" . FULL_RELEASE_VERSION . \".\" . RPM_DISTRO . \".\" . RPM_ARCH . \".rpm\");\n\n\nif (!file_exists(APP_META_FILE)) {\n    die(\"NO APP-META. EXIT.\");\n}\n\nif (!BUILD_NUMBER) {\n    die(\"This script must be called from Jenkins___\");\n}\n$appmeta = simplexml_load_file(APP_META_FILE);\n$inifile = parse_ini_file(DOCKER_INI_FILE);\necho sprintf(\"Creating Docker File for application: %s\\n\", APS_NAME);\n\n$downloadurl =\n    INGRAM_DEV_CATALOG_STORAGE . VENDOR_NAME . \"/\" . APS_NAME . \"/\" . FULL_RELEASE_VERSION . \"/\" . PACKAGER_NAME .\n    \"/undefined/undefined/undefined/resources/endpoint/\" . RPM_NAME;\n\n\necho sprintf(\"Download location will be: %s\\n\", $downloadurl);\n\n$fileData = sprintf(\n    \"FROM %s:%s\\nMAINTAINER SIT Builder <sitbuilder@sofcloudit.com>\\n\", $inifile['os'], $inifile['osversion']\n);\n\nforeach ($inifile['run'] as $run) {\n    $fileData .= sprintf(\"RUN %s\\n\", $run);\n}\n\n$fileData .= sprintf(\"RUN yum -y install %s\\n\", $downloadurl);\n$fileData .= 'RUN ln -sf /proc/self/fd/1 /var/log/httpd/ssl_error_log && ln -sf /proc/self/fd/1 /var/log/httpd/error_log && ln -sf /proc/self/fd/1 /var/log/aps/___app___.log && ln -sf /proc/self/fd/1 /var/log/httpd/access_log && ln -sf /proc/self/fd/1 /var/log/httpd/ssl_access_log' . \"\\n\";\n$fileData .= 'CMD [\"/usr/sbin/httpd\", \"-D\", \"FOREGROUND\"]' . \"\\n\";\n\nforeach ($inifile['ports'] as $port) {\n    $fileData .= sprintf(\"EXPOSE %s\\n\", $port);\n}\n\n$fileData .= sprintf(\"VOLUME [\\\"/var/www/html/%s/config\\\"]\\n\", $inifile['rpmPrefix']);\n\nfile_put_contents(DOCKER_FILE, $fileData);\necho \"\\nCreated Docker File!\\n\";\n\n\necho \"Creating APS Docker Info\\n\";\n$dockermeta = new SimpleXMLElement(\"<deployment></deployment>\");\n$dockermeta->addChild('imageName', \"odin-ext/\" . strtolower($inifile['rpmPrefix']));\n$dockermeta->addChild('applicationPort', '443');\n$dockermeta->addChild('applicationUrlPath', $inifile['rpmPrefix']);\n$dockermeta->asXml(DEPLOYMENT_XML_FILE);"
CHANGELOGFILE = "<?php\n/*\n * Copyright © 2018 Ingram Micro Inc.  Any rights not granted herein are reserved for Ingram Micro Inc.\n * Permission to use, copy and distribute this source code without fee and without a signed license agreement is\n * hereby granted provided that: (i) the above copyright notice and this paragraph appear in all copies and\n * distributions; and (ii) the source code is only used, copied or distributed for the purpose of using it with\n * the APS package for which Ingram Micro Inc. or its affiliates integrated it\n * into. Ingram Micro Inc. may revoke the limited license granted herein at any time at its sole discretion.\n *\n * THIS SOURCE CODE IS PROVIDED AS IS. INGRAM MICRO INC. MAKES NO REPRESENTATIONS OR WARRANTIES AND DISCLAIMS ALL\n * IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE.\n *\n */\n\n$app_meta = file_get_contents($argv[1]);\n$spec_file = file_get_contents($argv[2]);\ndate_default_timezone_set(\"UTC\");\n$date = new \DateTime();\n\n$dom = new DOMDocument('1.0');\n$dom->loadXML(simplexml_load_string($app_meta)->asXML());\n$changelog = $dom->getElementsByTagName('changelog');\nif ($changelog->length === 0) {\n    echo \"No changelog found in APP-META\", PHP_EOL;\n    die;\n}\n\n$rpm_changelog = array(\"\n\", \"%changelog\", \"\n\");\nforeach ($changelog->item(0)->getElementsByTagName('version') as $entry) {\n    $attributes = array();\n    foreach ($entry->attributes as $attribute) {\n        $attributes[$attribute->nodeName] = $attribute->nodeValue;\n    }\n    $new_arr = array_map('trim', explode(\"\n\", trim($entry->nodeValue)));\n    array_push($rpm_changelog,\n        \"* \" . $date->format(\"D M d Y\") . \" INGRAM MICRO EUROPEAN SERVICES SL <builder@sofcloudit.com> - \" .\n        $attributes[\"version\"] . \"-\" .\n        $attributes[\"release\"] . \"\n\" . implode(\"\n\", $new_arr) . \"\n\n\"\n    );\n}\n\nif (count($rpm_changelog) > 3) {\n    file_put_contents($argv[2], $spec_file . implode(\"\", $rpm_changelog));\n    echo \"Changelog generated\", PHP_EOL;\n}\n"
RPMSFILE = "#!/bin/bash\nset -e\nset -x\n\nPHP_EXEC=/usr/bin/php\nPERL_EXEC=/usr/bin/perl\nCUT_EXEC=/usr/bin/cut\nFIND_EXEC=/usr/bin/find\nXARGS_EXEC=/usr/bin/xargs\nSED_EXEC=/bin/sed\nWGET_EXEC=/usr/bin/wget\nRM_EXEC=/usr/bin/rm\nMKDIR_EXEC=/usr/bin/mkdir\nLS_EXEC=/usr/bin/ls\nCP_EXEC=/usr/bin/cp\nDU_EXEC=/usr/bin/du\nIONCUBE_EXEC=/usr/ioncube/ioncube_encoder54_9.0_64\nPMAKE_EXEC=/bin/pmake\nDOCKERAUTH_EXEC=/usr/bin/dockerauth\nDOCKER_EXEC=/bin/docker\nSUDO_EXEC=/bin/sudo\nCOMPOSER_URL=https://getcomposer.org/composer.phar\nINIT_PLACE=$PWD # this is builder folder\nAPS_ROOT=\"${INIT_PLACE}/..\" # this is APS root folder schemas,scripts,docs...\nSCHEMAS_DIR=${APS_ROOT}/schemas # this is APS root folder schemas,scripts,docs...\nENDPOINT_DIR=${APS_ROOT}/endpoint # this is APS root folder schemas,scripts,docs...\nSCRIPTS_DIR=${APS_ROOT}/scripts # this is APS root folder schemas,scripts,docs...\nAPS_TYPES_FILE=${INIT_PLACE}/aps-types.ini\nAPS_VERSION=2.2\n\necho \"Doing Build for version ${BUILD_NUMBER} and branch ${GIT_BRANCH}\"\n\ncd ${APS_ROOT}/scripts\n${WGET_EXEC} ${COMPOSER_URL} -O ${SCRIPTS_DIR}/composer.phar -o /dev/null\n${PHP_EXEC} ${SCRIPTS_DIR}/composer.phar install --no-interaction --no-ansi\n${PHP_EXEC} ${SCRIPTS_DIR}/composer.phar update --no-dev --no-interaction --no-ansi\n${RM_EXEC} -f ${SCRIPTS_DIR}/composer.phar\n\necho \"Replacing APS Types\"\n${PERL_EXEC} -i -p -e 's/\\r\\n/\\n/g' ${APS_TYPES_FILE}\nsource <(grep = ${APS_TYPES_FILE} | sed 's/ = /=/g')\n${CUT_EXEC} -d= -f1 ${APS_TYPES_FILE} | while read aps_type_var; do ${FIND_EXEC} ${APS_ROOT}/{scripts/,ui/,APP-META.xml} -type f | ${XARGS_EXEC} ${SED_EXEC} -i -e \"s@${aps_type_var}@${!aps_type_var}@g\"; done\n\n${MKDIR_EXEC} -p ${SCHEMAS_DIR}\n${MKDIR_EXEC} -p ${ENDPOINT_DIR}\n\n# aps msgmake -l en_US ${APS_ROOT}\n\necho \"Building Schemas\"\ndeclare -a resourcesarr=(\"___app___Resource1\", \"___app___Resource2\")\nfor i in \"${resourcesarr[@]}\"\ndo\n    ${PHP_EXEC} ${SCRIPTS_DIR}/${i}.php '$schema' > ${SCHEMAS_DIR}/${i}.schema.gen\n    SCHEMA=$(${PHP_EXEC} -r '$f=file_get_contents($argv[1]);$j=json_decode($f);$t=gettype($j);echo $t;' ${SCHEMAS_DIR}/${i}.schema.gen)\n    if [[ ${SCHEMA} == \"NULL\" ]]\n    then\n        echo \"Schema for ${i} was not generated properly. Existing.\"\n        exit 1\n    else\n        echo \"Decoded schema for ${i} is of PHP type: ${SCHEMA}\"\n    fi\ndone\n\necho \"Built\"\n\nset +e\n\n${RM_EXEC} -rf ${SCRIPTS_DIR}/lib/runtime/.git\n${LS_EXEC} -l ${SCHEMAS_DIR}\n\n${MKDIR_EXEC} -p $INIT_PLACE/tmp.el7/var/www/html/___app___/\n${MKDIR_EXEC} -p $INIT_PLACE/tmp.el7/etc/rsyslog.d/\n${MKDIR_EXEC} -p $INIT_PLACE/tmp.el7/etc/logrotate.d/\n${MKDIR_EXEC} -p $INIT_PLACE/tmp.el7/etc/php.d/\n${MKDIR_EXEC} -p $INIT_PLACE/tmp.el7/etc/httpd/conf.d\n\n${CP_EXEC} $INIT_PLACE/rsyslog/aps_logger.el7.conf $INIT_PLACE/tmp.el7/etc/rsyslog.d/___app___.conf\n${CP_EXEC} $INIT_PLACE/logrotate/___app___ $INIT_PLACE/tmp.el7/etc/logrotate.d/___app___\n${CP_EXEC} $INIT_PLACE/ioncube/ioncube____app___.ini  $INIT_PLACE/tmp.el7/etc/php.d/ioncube____app___.ini\n${CP_EXEC} $INIT_PLACE/httpd/___app___.conf $INIT_PLACE/tmp.el7/etc/httpd/conf.d/___app___.conf\n${CP_EXEC} -r ${APS_ROOT}/scripts $INIT_PLACE/tmp.el7/var/www/html/___app___/\n\n\nfor i in \"${resourcesarr[@]}\"\ndo\n    ${CP_EXEC} -r ${SCHEMAS_DIR}/${i}.schema.gen $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts/schemas/${i}.schema.gen\ndone\n\n${CP_EXEC} ${APS_ROOT}/APP-META.xml $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts/schemas/\n\n${LS_EXEC} -ld $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts\n${DU_EXEC} -sh $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts\n\n\necho \"Encrypting the build for PHP 5.4\"\n${IONCUBE_EXEC} $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts -o $INIT_PLACE/tmp.el7/var/www/html/___app___ --add-comment \"This file is part of Endpoint service for Mozy APS Distributive ${APS_VERSION}-${BUILD_NUMBER}\" --add-comment \"Copyright (c) 2018 Ingram Micro\" --message-if-no-loader \"'Endpoint service for Mozy requires ionCube loader'\" --obfuscate 'none' --allow-reflection-all --loader-path \"'/var/www/html/___app___/lib/loaders/'\" --update-target\necho \"${IONCUBE_EXEC} EXIT CODE: $?\"\n${RM_EXEC} -rf $INIT_PLACE/tmp.el7/var/www/html/___app___/scripts\necho \"Encrypted for PHP 5.4\"\n${LS_EXEC} -l $INIT_PLACE/tmp.el7/var/www/html/___app___\n${DU_EXEC} -sh $INIT_PLACE/tmp.el7/var/www/html/___app___\n\n\n${PERL_EXEC} -pi -e 's/BUILDREMPLACE/'${BUILD_NUMBER}'/g' ${APS_ROOT}/APP-META.xml\n\necho \"Generating RPM changelog\"\n${PHP_EXEC} ${INIT_PLACE}/generate_changelog.php ${APS_ROOT}/APP-META.xml ${INIT_PLACE}/___app___.el7.x86_64.spec\n\ncd $INIT_PLACE/tmp.el7 && tar -cvzf ___app___-el7.tar.gz *\nmv $INIT_PLACE/tmp.el7/___app___-el7.tar.gz $INIT_PLACE\n\n${RM_EXEC} -rf $INIT_PLACE/tmp.el7\n\ncd $INIT_PLACE\ndeclare -a arr=(\"___app___.el7.x86_64.spec\")\nfor i in \"${arr[@]}\"\ndo\n    spec=`rpm --eval '%{_specdir}'/$i`\n    cat ${dir}$i | fgrep -v '%prep' | fgrep -v '%setup' > $spec\n\n    name=`rpm -q --specfile $spec --qf '%{name}\\n' | head -n1`\n    echo rpmbuild -bb $spec\n    td=`mktemp -d /var/tmp/${name}-XXXXXX`; trap \"rm -rf $td\" 0\n    set +e\n    rpmbuild --buildroot $td --define \"_builddir $PWD\" -bb $spec\n    rc=$?\n    rm -rf $td\n    rpmd=`rpm --eval '%{_rpmdir}'`\n    rpmf=`rpm -q --specfile $spec --qf '%{arch}/%{name}-%{version}-%{release}.%{arch}.rpm\\n'`\n    mv $rpmd/$rpmf ${ENDPOINT_DIR}/\ndone\n\ntrap \"\" 0\n${RM_EXEC} -rf ${SCRIPTS_DIR}\n\n\nif [[ ${CREATE_DOCKER} == \"YES\" ]]\nthen\n    cd ${INIT_PLACE}\n    echo \"Creating deployment.xml\"\n    ${PHP_EXEC} ${INIT_PLACE}/docker.php\nfi\n\ncd ${APS_ROOT}\n${PMAKE_EXEC} APSRELEASE=${BUILD_NUMBER} clean publish\n\nrc=$?\n\nif [ $? -eq 0 ]\n    then\n        if [[ ${CREATE_DOCKER} == \"YES\" ]]\n            then\n                echo \"Doing Docker Image\"\n                if [ $rc -eq 0 ]\n                    then\n                        prefix=$(awk -F \"=\" '/rpmPrefix/ {print $2}' ${INIT_PLACE}/docker.ini)\n                        prefix=\"${prefix//\\\"}\"\n                        prefix=\"${prefix// }\"\n                        prefix=\"$(tr [A-Z] [a-z] <<< \"$prefix\")\"\n                        echo \"doing build . -t gcr.io/odin-ext/\"${prefix}:${APS_VERSION}-${BUILD_NUMBER}\n                        cd ${INIT_PLACE}\n                        ${SUDO_EXEC} ${DOCKER_EXEC} build . -t gcr.io/odin-ext/${prefix}:${APS_VERSION}-${BUILD_NUMBER}\n                        rc=$?\n                        echo \"Output of docker build:\"$rc\n                        if [ $rc -eq 0 ]\n                            then\n                            ${SUDO_EXEC} ${DOCKERAUTH_EXEC}\n                            ${SUDO_EXEC} ${DOCKER_EXEC} push gcr.io/odin-ext/${prefix}:${APS_VERSION}-${BUILD_NUMBER}\n                        fi\n                        rc=$?\n                fi\n            fi\nfi\nif [ $rc -eq 0 ]\nthen\n    exit $rc\nelse\n    exit -1\nfi"
GUIPARSERFILE = "define([\n    \"aps/i18n\",\n    \"dojox/mvc/at\"\n], function (\n    _,\n    at\n) {\n    \"use strict\";\n\n    var replaceProperty_r = function (properties, key, view) {\n        var innerKey;\n        var stripped;\n\n        // Replace \"@\" prefix with at function\n        if (typeof properties[key] === \"string\" && properties[key][0] === \"@\") {\n            var parts = properties[key].slice(1).split(\",\");\n            if (parts.length > 1) {\n                properties[key] = at(parts[0].trim(), parts[1].trim());\n            }\n        }\n\n        // Replace \"_\" prefix with _ function\n        else if (typeof properties[key] === \"string\" && properties[key][0] === \"_\") {\n            stripped = properties[key].slice(1);\n            properties[key] = _(stripped);\n        }\n\n        // Replace \"!\" prefix with function object\n        else if (typeof properties[key] === \"string\" && properties[key][0] === \"!\") {\n            stripped = properties[key].slice(1);\n            if (view && typeof view[stripped] === \"function\") {\n                properties[key] = view[stripped];\n            } else if (!view && typeof window[stripped] === \"function\") {\n                properties[key] = window[stripped];\n            } else {\n                console.warn(\"Function '\" + stripped +\n                    \"' does not exist in view \" + JSON.stringify(view) +\n                    \" nor in global context\");\n            }\n        }\n\n        // If property contains an object, replace subproperties\n        else if (typeof properties[key] === \"object\") {\n            for (innerKey in properties[key]) {\n                if (properties[key].hasOwnProperty(innerKey)) { // PHPStorm complains if I don't check this\n                    replaceProperty_r(properties[key], innerKey, view);\n                }\n            }\n        }\n\n        // If property contains array of objects, replace properties within the objects\n        else if (Array.isArray(properties[key])) {\n            for (var i = 0; i < properties[key].length; ++i) {\n                var object = properties[key][i];\n                if (typeof object === \"object\") {\n                    for (innerKey in object) {\n                        if (object.hasOwnProperty(innerKey)) { // PHPStorm complains if I don't check this\n                            replaceProperty_r(object, innerKey, view);\n                        }\n                    }\n                }\n            }\n        }\n    };\n\n    var replaceProperties_r = function (widget, view) {\n        var properties = widget[1];\n        var children = widget[2];\n\n        // Check that properties is not an array (because some widgets do not have properties\n        // and second entry contains the children)\n        if (properties && !Array.isArray(properties)) {\n            // Generate unique id\n            if (view && properties.hasOwnProperty(\"id\")) {\n                properties.id = view.genId(properties.id);\n            }\n\n            // Replace properties\n            for (var key in properties) {\n                if (properties.hasOwnProperty(key)) { // PHPStorm complains if I don't check this\n                    replaceProperty_r(properties, key, view);\n                }\n            }\n        } else {\n            // If second element was not the properties object, then it was the children array\n            children = properties;\n        }\n\n        // Replace id's in children\n        if (children) {\n            for (var i = 0; i < children.length; ++i) {\n                var child = children[i];\n                replaceProperties_r(child, view);\n            }\n        }\n    };\n\n    /**\n     *\n     * @constructor\n     */\n    var GuiParser = function () {\n    };\n\n    /**\n     * Parses a JSON gui\n     * The parser automatically performs the following conversions:\n     * * 'id' property: Replaces the string value with view.genId(value)\n     * * String values in the format \"@a,b\": Replaces the value with at(a, b)\n     * * String values that begin with \"_\": Localizes the string\n     * * String values that begin with \"!\": Replaces with the function object view[value]\n     * @param {string} jsonGui\n     * @param {aps/View} view\n     * @returns {Array}\n     */\n    GuiParser.parse = function (jsonGui, view) {\n        var gui = JSON.parse(jsonGui);\n        replaceProperties_r(gui, view);\n        return gui;\n    };\n\n    return GuiParser;\n});\n"

# ---------------------------
# parse_args_or_exit
# Parses the arguments and returns json filename
# If number or arguments is incorrect, prints
# error message and exits program
# ---------------------------

def parse_args_or_exit():
    if len(sys.argv) != 2:
        print("ERROR: Specify json schema as command line argument")
        sys.exit()
    return sys.argv[1]

# ---------------------------
# create_project
# Creates the project
# ---------------------------

def create_project(project_name, json_object):
    services = json_object["services"]
    has_init_wizard = True if "initWizard" in json_object else False
    has_composer_json = True if "composer" in json_object else False
    types = json_object["types"] if "types" in json_object else {}
    constants = json_object["constants"] if "constants" in json_object else {}
    navigation_objects = json_object["navigation"] if "navigation" in json_object else []
    
    if not os.path.exists(project_name):
        os.makedirs(project_name)
    create_app_meta(project_name, services, navigation_objects)
    create_makefile(project_name)
    create_builder(project_name, types)
    create_scripts(project_name, services, has_init_wizard, types, constants)
    create_ui(project_name, navigation_objects)
    if has_init_wizard:
        create_init_wizard(project_name, json_object["initWizard"])
    create_tests(project_name, services)
    if has_composer_json:
        create_composer_json(project_name, json_object["composer"])

# ---------------------------
# create_app_meta
# ---------------------------

def create_app_meta(project_name, services, navigation_objects):
    # Do not overwrite existing meta file
    if not os.path.isfile(project_name + "/APP-META.xml"):
        with open(project_name + "/APP-META.xml", "w") as file_handle:
            file_handle.write('<application xmlns="http://aps-standard.org/ns/2" version="2.0">\n')
            file_handle.write('    <id>http://www.' + project_name + '.com</id>\n')
            file_handle.write('    <name>' + project_name + '</name>\n')
            file_handle.write('    <version>1.0</version>\n    <release>0</release>\n')
            file_handle.write('    <homepage>http://' + project_name + '.com</homepage>\n')
            file_handle.write('    <vendor>\n        <name>Ingram Micro</name>\n        <homepage>https://www.ingrammicro.com</homepage>\n    </vendor>\n')
            file_handle.write('    <packager>\n        <name>Ingram Micro</name>\n        <homepage>https://www.ingrammicro.com</homepage>\n    </packager>\n\n')
            file_handle.write('    <presentation>\n')
            file_handle.write('        <summary>' + project_name + '</summary>\n')
            file_handle.write('        <description>' + project_name + '</description>\n')
            file_handle.write('        <categories>\n            <category>' + project_name + '</category>\n        </categories>\n')

            for navigation in navigation_objects:
                ccpv1 = navigation["ccpv1"] if "ccpv1" in navigation else False
                file_handle.write('        <navigation id="' + navigation["id"] + '" label="' + navigation["label"] + '">\n')
                file_handle.write('            <plugs-to id="' + navigation["plugs-to"] + '"/>\n')
                for item in navigation["items"]:
                    view = item["view"]
                    controls = view["controls"] if "controls" in view else None
                    file_handle.write('            <item id="' + item["id"] + '" label="' + item["label"] + '">\n')
                    file_handle.write('                <view id="' + view["id"] + '" label="' + view["label"] + '" src="ui/' + view["id"])
                    file_handle.write('.html">\n' if ccpv1 else '.js">\n')
                    if controls:
                        file_handle.write('                <controls>\n')
                        for control_type, control_label in controls.items():
                            file_handle.write('                    <' + control_type + ' label="' + control_label + '"/>\n')
                        file_handle.write('                </controls>\n')
                    file_handle.write('                </view>\n')
                    file_handle.write('            </item>\n')
                file_handle.write('        </navigation>\n')
            
            if len(navigation_objects) == 0:
                file_handle.write('        <navigation id="ccp" label="' + project_name + '">\n')
                file_handle.write('            <plugs-to id="http://www.parallels.com/ccp/2"/>\n')
                file_handle.write('            <item id="main-item" label="Main Item">\n')
                file_handle.write('                <view id="autogen-view" label="Main View" src="ui/autogen-view.js">\n                </view>\n')
                file_handle.write('            </item>\n')
                file_handle.write('        </navigation>\n')
            
            file_handle.write('    </presentation>\n\n')
            file_handle.write('    <license-agreement must-accept="true">\n')
            file_handle.write('    <free/>\n')
            file_handle.write('    <text>\n')
            file_handle.write('        <name>End-User License Agreement</name>\n')
            file_handle.write('        <file>http://opensource.org/licenses/bsd-license</file>\n')
            file_handle.write('    </text>\n')
            file_handle.write('    </license-agreement>\n\n')
            file_handle.write('    <upgrade match="version=ge=1.0, release=ge=0"/>\n\n')
            
            for service in services:
                file_handle.write('    <service id="' + service["name"] + '">\n')
                file_handle.write('        <code engine="php" path="scripts/' + service["name"] + '.php" />\n')
                file_handle.write('        <presentation>\n')
                file_handle.write('            <name>' + service["name"] + '</name>\n')
                file_handle.write('            <summary>' + service["name"] + '</summary>\n')
                file_handle.write('        </presentation>\n')
                file_handle.write('    </service>\n\n')
            file_handle.write('</application>')

# ---------------------------
# create_makefile
# ---------------------------

def create_makefile(project_name):
    with open(project_name + "/Makefile", "w") as file_handle:
        file_handle.write("REPLACE= yes\n\n")
        file_handle.write("FILES!= find deployment.xml i18n docs endpoint images schemas ui -type f | grep -v '.git'\n\n")
        file_handle.write(".include <aps.package.mk>\n")

# ---------------------------
# create_builder
# ---------------------------

def create_builder(project_name, types):
    builder_path = project_name + "/builder"

    # Create folder
    if not os.path.exists(builder_path):
        os.makedirs(builder_path)

    # Create aps types file
    with open(builder_path + "/aps-types.ini", "w") as file_handle:
        for type_mask, type_name in types.items():
            file_handle.write(type_mask.strip() + '=' + type_name.strip() + '\n')
    
    # Create spec file
    with open(builder_path + "/" + project_name + ".el7.x86_64.spec", "w") as file_handle:
        file_handle.write(SPECFILE.replace("___app___", project_name.replace("-", "_")))
    
    # Create docker ini file
    with open(builder_path + "/docker.ini", "w") as file_handle:
        file_handle.write(DOCKERINIFILE.replace("___app___", project_name.replace("-", "_")))

    # Create docker php file
    with open(builder_path + "/docker.php", "w") as file_handle:
        file_handle.write(DOCKERPHPFILE.replace("___app___", project_name.replace("-", "_")))

    # Create changelog file
    with open(builder_path + "/generate_changelog.php", "w") as file_handle:
        file_handle.write(CHANGELOGFILE.replace("___app___", project_name.replace("-", "_")))

    # Create rpms file
    with open(builder_path + "/rpms.sh", "w") as file_handle:
        file_handle.write(RPMSFILE.replace("___app___", project_name.replace("-", "_")))

# ---------------------------
# create_scripts
# ---------------------------

def create_scripts(project_name, services, has_init_wizard, types, constants):
    scripts_path = project_name + "/scripts"

    # Create folder
    if not os.path.exists(scripts_path):
        os.makedirs(scripts_path)

    # Create script
    for service in services:
        is_application = False
        filename = scripts_path + "/" + service["name"] + ".php"
        structures = service["structures"] if "structures" in service else {}
        relations = service["relations"] if "relations" in service else {}
        properties = service["properties"] if "properties" in service else {}
        operations = service["operations"] if "operations" in service else {}

        # Do not overwrite existing scripts
        if not os.path.isfile(filename):
            with open(filename, "w") as file_handle:
                # Write header
                file_handle.write('<?php\n\n')
                file_handle.write("define('APS_DEVELOPMENT_MODE', true);\n")
                file_handle.write('require_once "includes.php";\n\n')

                # Write structures
                for structure_name, structure_obj in structures.items():
                    file_handle.write('class ' + structure_name + '\n{\n')
                    props = structure_obj["properties"]
                    for property_name, property_obj in props.items():
                        file_handle.write('    /**\n     * @type("' + property_obj["type"] + '")\n')
                        file_handle.write('     * @title("' + property_obj["title"] + '")\n')
                        file_handle.write('     * @description("' + property_obj["description"] + '")\n')
                        file_handle.write('     */\n')
                        file_handle.write('    public $' + property_name + ';\n\n')
                    file_handle.write('}\n\n')

                # Write class start
                file_handle.write('/**\n * @type("' + service["id"] + '")\n')
                for implements_name in service["implements"]:
                    file_handle.write(' * @implements("' + implements_name + '")\n')
                    implements_name = types[implements_name] if implements_name in types else implements_name
                    if implements_name == "http://aps-standard.org/types/core/application/1.0":
                        is_application = True
                file_handle.write(' */\n')
                file_handle.write('class ' + service["name"] + ' extends \\APS\\ResourceBase\n{\n')
                
                # Write relations
                for relation_name, relation in relations.items():
                    file_handle.write('    /**\n     * @link("' + relation["type"])
                    if "collection" in relation and relation["collection"]: file_handle.write('[]')
                    file_handle.write('")\n')
                    if "required" in relation and relation["required"]: file_handle.write('     * @required\n')
                    file_handle.write('     */\n')
                    file_handle.write('    public $' + relation_name + ';\n\n')
                
                # Write properties
                for property_name, property_obj in properties.items():
                    if property_obj["type"] == "array":
                        array_type = property_obj["items"]["type"]
                        file_handle.write('    /**\n     * @type("' + array_type + '[]")\n')
                    else:
                        file_handle.write('    /**\n     * @type("' + property_obj["type"] + '")\n')
                    file_handle.write('     * @title("' + property_obj["title"] + '")\n')
                    file_handle.write('     * @description("' + property_obj["description"] + '")\n')
                    file_handle.write('     */\n')
                    file_handle.write('    public $' + property_name + ';\n\n')
                
                # Write methods
                file_handle.write('    public function provision()\n    {\n        \n    }\n\n')
                file_handle.write('    public function configure($new)\n    {\n        \n    }\n\n')
                file_handle.write('    public function retrieve()\n    {\n        \n    }\n\n')
                file_handle.write('    public function upgrade()\n    {\n        \n    }\n\n')
                file_handle.write('    public function unprovision()\n    {\n        \n    }\n\n')

                # Write init wizard methods
                if has_init_wizard and is_application:
                    #file_handle.write('    /**\n     * @verb(GET)\n')
                    #file_handle.write('     * @path("/getInitWizardConfig")\n')
                    #file_handle.write('     * @access(admin, true)\n')
                    #file_handle.write('     * @access(owner, true)\n')
                    #file_handle.write('     * @access(referrer, true)\n')
                    #file_handle.write('     */\n')
                    file_handle.write('    public function getInitWizardConfig()\n    {\n')
                    file_handle.write('        $myfile = fopen("./wizard_data.json", "r") or die("Unable to open file!");\n')
                    file_handle.write('        $data = fread($myfile, filesize("./wizard_data.json"));\n')
                    file_handle.write('        fclose($myfile);\n')
                    file_handle.write('        return json_decode($data);\n')
                    file_handle.write('    }\n\n')
                    file_handle.write('    public function testConnection()\n    {\n        return "";\n    }\n\n')

                # Write custom operations
                for operation_name, operation_obj in operations.items():
                    file_handle.write('    /**\n     * @verb(' + operation_obj["verb"] + ')\n')
                    file_handle.write('     * @path(' + operation_obj["path"] + ')\n')
                    response = operation_obj["response"]
                    file_handle.write('     * @return(' + response["type"] + ',' + response["contentType"] + ')\n')
                    file_handle.write('     */\n')
                    file_handle.write('    public function ' + operation_name + '()\n    {\n        \n    }\n\n')
                
                # Write class end
                file_handle.write('}\n')

    # Create includes.php
    with open(scripts_path + "/includes.php", "w") as file_handle:
        file_handle.write('<?php\n\n')

        # Add constants
        for constant_name, constant_value in constants.items():
            file_handle.write('define("' + constant_name + '", "' + constant_value  + '");\n')
        if len(constants) > 0:
            file_handle.write('\n')
        
        # Add requires
        file_handle.write('include_once "vendor/autoload.php";\n')
        file_handle.write('require_once "aps/2/runtime.php";\n\n')

        # Add requires for services
        #for service in services:
        #    file_handle.write('require_once "' + service["name"] + '.php";\n')
        #file_handle.write('\n')
        
        # Guard to make it work when unit testing
        # (fails when including it for unit tests if not present)
        file_handle.write('if (isset($_SERVER["SCRIPT_FILENAME"]) && strpos($_SERVER["SCRIPT_FILENAME"], "phpunit") !== false) {\n')
        file_handle.write('    define("APS_RUNTIME_NO_PROCESS", true);\n')
        file_handle.write('}\n')

# ---------------------------
# create_ui
# ---------------------------

def create_ui(project_name, navigation_objects):
    ui_path = project_name + "/ui"
    json_path = ui_path + "/json"

    # Create folders
    if not os.path.exists(ui_path):
        os.makedirs(ui_path)
    if not os.path.exists(json_path):
        os.makedirs(json_path)

    # Create gui parser
    filename_guiparser = ui_path + "/GuiParser.js"
    if not os.path.isfile(filename_guiparser):
        with open(filename_guiparser, "w") as file_handle:
            file_handle.write(GUIPARSERFILE)

    # Create ui scripts
    for navigation in navigation_objects:
        ccpv1 = navigation["ccpv1"] if "ccpv1" in navigation else False
        for item in navigation["items"]:
            view = item["view"]
            filename_json = json_path + "/" + view["id"] + ".json"
            filename_js = ui_path + "/" + view["id"] + ".js"
            filename_html = ui_path + "/" + view["id"] + ".html" if ccpv1 else None
            callbacks = parse_properties_r(view["contents"])

            # Do not overwrite existing json file
            if not os.path.isfile(filename_json):
                with open(filename_json, "w") as file_handle:
                    json.dump(view["contents"], file_handle, indent = 4, ensure_ascii = False)
            
            # Write CCPv1 html script
            if ccpv1:
                with open(filename_html, "w") as file_handle:
                    file_handle.write('<!DOCTYPE html>\n')
                    file_handle.write('<html>\n')
                    file_handle.write('<head>\n')
                    file_handle.write('    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">\n')
                    file_handle.write('    <script src="/aps/2/ui/runtime/client/aps/aps.js"></script>\n')
                    file_handle.write('    <script type="text/javascript" src="' + view["id"] + '.js"></script>\n')
                    file_handle.write('</head>\n')
                    file_handle.write('<body>\n')
                    file_handle.write('</body>\n')
                    file_handle.write('</html>\n')
            
            # Write CCPv1 js script (do not overwrite existing file)
            if ccpv1 and not os.path.isfile(filename_js):
                with open(filename_js, "w") as file_handle:
                    file_handle.write('require([\n')
                    file_handle.write('    "./GuiParser.js",\n')
                    file_handle.write('    "aps/load",\n')
                    file_handle.write('    "dojo/text!./json/' + view["id"] + '.json",\n')
                    file_handle.write('    "aps/ready!"\n')
                    file_handle.write('],\n')
                    file_handle.write('function(\n')
                    file_handle.write('    GuiParser,\n')
                    file_handle.write('    load,\n')
                    file_handle.write('    widgets\n')
                    file_handle.write(') {\n')
                    file_handle.write('    "use strict";\n\n')
                    file_handle.write('    load(GuiParser.parse(widgets, null));\n')
                    file_handle.write('});\n')

                    # Export callbacks
                    file_handle.write('\n' if len(callbacks) > 0 else '\n')
                    for i, callback in enumerate(callbacks):
                        file_handle.write('\nfunction ' + callback + '() {\n')
                        file_handle.write('    alert("' + callback + ' callback");\n')
                        file_handle.write('}\n')
            
            # Write UX1 js script (do not overwrite existing file)
            if not ccpv1 and not os.path.isfile(filename_js):
                with open(filename_js, "w") as file_handle:
                    file_handle.write('define([\n')
                    file_handle.write('    "./GuiParser.js",\n')
                    file_handle.write('    "aps/View",\n')
                    file_handle.write('    "dojo/_base/declare",\n')
                    file_handle.write('    "dojo/text!./json/' + view["id"] + '.json"\n')
                    file_handle.write('],\n')
                    file_handle.write('function (\n')
                    file_handle.write('    GuiParser,\n')
                    file_handle.write('    View,\n')
                    file_handle.write('    declare,\n')
                    file_handle.write('    widgets\n')
                    file_handle.write(') {\n')
                    file_handle.write('    "use strict";\n\n')
                    file_handle.write('    var self;\n\n')
                    file_handle.write('    return declare(View, {\n')
                    file_handle.write('        init: function () {\n')
                    file_handle.write('            self = this;\n')
                    file_handle.write('            return GuiParser.parse(widgets, self);\n')
                    file_handle.write('        },\n\n')
                    file_handle.write('        onContext: function () {\n')
                    file_handle.write('            aps.apsc.hideLoading();\n')
                    file_handle.write('        }')
                    
                    # Export callbacks
                    file_handle.write(',\n\n' if len(callbacks) > 0 else '\n')
                    for i, callback in enumerate(callbacks):
                        file_handle.write('        ' + callback + ': function() {\n')
                        file_handle.write('            alert("' + callback + ' callback");\n')
                        file_handle.write('        }')
                        file_handle.write(',\n\n' if i < len(callbacks)-1 else '\n')

                    file_handle.write('    });\n')
                    file_handle.write('});\n')

    # Create default ui script if no navigation objects are defined
    if len(navigation_objects) == 0:
        filename_js = ui_path + "/autogen-view.js"

        # Do not overwrite existing ui script
        if not os.path.isfile(filename_js):
            with open(filename_js, "w") as file_handle:
                file_handle.write('define([\n')
                file_handle.write('    "dojo/_base/declare",\n')
                file_handle.write('    "aps/View"\n')
                file_handle.write('],\n')
                file_handle.write('function (\n')
                file_handle.write('    declare,\n')
                file_handle.write('    View\n')
                file_handle.write(') {\n')
                file_handle.write('    var self;\n\n')
                file_handle.write('    return declare(View, {\n')
                file_handle.write('        init: function () {\n')
                file_handle.write('            self = this;\n\n')
                file_handle.write('            return [\n')
                file_handle.write('                "aps/Output",\n')
                file_handle.write('                {\n')
                file_handle.write('                    content: "Hello from ' + project_name + ' autogenerated view!"\n')
                file_handle.write('                }\n')
                file_handle.write('            ];\n')
                file_handle.write('        },\n\n')
                file_handle.write('        onContext: function () {\n')
                file_handle.write('            aps.apsc.hideLoading();\n')
                file_handle.write('        }\n')
                file_handle.write('    });\n')
                file_handle.write('});\n')

# ---------------------------
# create_init_wizard
# ---------------------------

def create_init_wizard(project_name, init_wizard):
    with open(project_name + "/scripts/wizard_data.json", "w") as file_handle:
        json.dump(init_wizard, file_handle, indent = 4, ensure_ascii = False)

# ---------------------------
# create_tests
# ---------------------------

def create_tests(project_name, services):
    scripts_path = project_name + "/scripts"
    tests_path = scripts_path + "/tests"

    # Create folder
    if not os.path.exists(tests_path):
        os.makedirs(tests_path)

    # Create tests
    for service in services:
        filename = tests_path + "/" + service["name"] + "Test.php"
        operations = service["operations"] if "operations" in service else {}

        # Do not overwrite existing scripts
        if not os.path.isfile(filename):
            with open(filename, "w") as file_handle:
                # Write header
                file_handle.write('<?php\n\n')
                file_handle.write('require_once "' + service["name"] + '.php";\n\n')

                # Write class start
                file_handle.write('final class ' + service["name"] + 'Test extends \\PHPUnit\\Framework\\TestCase\n{\n')

                # Write setup method
                file_handle.write('    protected function setUp()\n    {\n        \n    }\n\n')

                # Write dummy test
                file_handle.write('    public function testLoveIsLove()\n    {\n        echo "All you need is love" . PHP_EOL;\n    }\n\n')

                # Write tests for custom operations
                for operation_name, operation_obj in operations.items():
                    file_handle.write('    public function test' + operation_name[0].upper() + operation_name[1:] + '()\n    {\n        \n    }\n\n')
                
                # Write class end
                file_handle.write('}\n')
    
    # Create phpunit.xml fle
    with open(scripts_path + "/phpunit.xml", "w") as file_handle:
        file_handle.write('<phpunit bootstrap="includes.php"\n')
        file_handle.write('        colors="true"\n')
        file_handle.write('        verbose="true"\n')
        file_handle.write('        stopOnError="false"\n')
        file_handle.write('        stopOnFailure="false">\n')
        file_handle.write('    <testsuites>\n')
        file_handle.write('        <testsuite name="' + project_name + ' Test Suite">\n')
        file_handle.write('            <directory>tests/</directory>\n')
        file_handle.write('        </testsuite>\n')
        file_handle.write('    </testsuites>\n')
        file_handle.write('</phpunit>\n')

# ---------------------------
# create_composer_json
# ---------------------------

def create_composer_json(project_name, composer_json):
    with open(project_name + "/scripts/composer.json", "w") as file_handle:
        json.dump(composer_json, file_handle, indent = 4, ensure_ascii = False)

# ---------------------------
# parse_properties_r
# ---------------------------

def parse_properties_r(widget):
    properties = widget[1] if len(widget) > 1 else None
    children = widget[2] if len(widget) > 2 else None
    methods_list = []

    # Check that properties is not an array (because some widgets do not have properties
    # and second entry contains the children)
    if properties and not isinstance(properties, list):
        # Parse properties
        for key, value in properties.items():
            func_name = parse_property_r(value)
            if func_name:
                methods_list.append(func_name)
    else:
        children = properties
    
    # Parse properties in children
    if children:
        for child in children:
            methods_list += parse_properties_r(child)
    
    return methods_list

# ---------------------------
# parse_property_r
# ---------------------------

def parse_property_r(property):
    # Return function name
    if isinstance(property, str) and len(property) > 0 and property[0] == "!":
        return property[1:]
    else:
        return None

# ---------------------------
# Main program
# ---------------------------

# Parse command line
json_filename = parse_args_or_exit()
project_name = os.path.splitext(json_filename)[0]

try:
    # Load json
    with open(json_filename) as file_handle:
        json_object = json.load(file_handle)

    # Create project
    create_project(project_name, json_object)
except FileNotFoundError:
    print("ERROR: Could not open '" + json_filename + "' schema")
