# aps-template
A Python script to generate a new APS project.

## TODO
- [x] Replace types script.
- [x] Undo replace types script.
- [x] Parse schemas to generate resources.
- [x] Remove template folder.
- [x] Support properties, structures and operations in the resource model.
- [x] Add init wizard.
- [x] Support UI definition.
- [x] Generate unit tests for services.
- [x] Generate composer.json file in scripts.
- [ ] Put default UI, makefile and meta contents on constants.
- [ ] Add support for @required on properties?
- [ ] Make replace types optional.
- [ ] In custom operations, support other response types than string.