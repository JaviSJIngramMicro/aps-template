#!/usr/bin/python3

import os
import sys

# ---------------------------
# parse_args_or_exit
# Parses the arguments and returns project name
# If number or arguments is incorrect, prints
# error message and exits program
# ---------------------------

def parse_args_or_exit():
    if len(sys.argv) != 2:
        print("ERROR: Specify project name as command line argument")
        sys.exit()
    
    project_name = sys.argv[1]
    if project_name[-1] == "/":
        project_name = project_name[:-1]

    if project_name == "template":
        print("ERROR: Cannot replace types on base template")
        sys.exit()
    
    return project_name

# ---------------------------
# parse_types_ini_or_exit
# Parse builder/aps-types.ini file to generate a list with all types
# ---------------------------

def parse_types_ini_or_exit(path):
    try:
        # Read file
        with open(path + "/builder/aps-types.ini", "r") as file_handle:
            lines = file_handle.readlines()
    
        # Split lines by "="
        types = [line.split("=") for line in lines]

        # Return dictionary of types
        return {type_[0].strip(): type_[1].strip() for type_ in types}
    except FileNotFoundError:
        print("ERROR: " + path + "/builder/aps-types.ini file does not exist")
        sys.exit()

# ---------------------------
# process_files
# Replace all types
# ---------------------------

def process_files(path, types):
    for root, dirs, files in os.walk(path):
        for f in files:
            process_file(root, f, types)

# ---------------------------
# process_file
# Processes a file's input and exports it
# ---------------------------

def process_file(path, file, types):
    # Guard against replacing aps-types.ini
    if file == "aps-types.ini":
        return

    try:
        # Read file
        with open(path + "/" + file, "r") as file_handle:
            file_contents = file_handle.read()

        # Process file contents
        for type_name, type_value in types.items():
            file_contents = file_contents.replace(type_name, type_value)

        # Write file
        with open(path + "/" + file, "w") as file_handle:
            file_handle.write(file_contents)
    # Non text files are ignored
    except UnicodeDecodeError:
        pass

# ---------------------------
# Main program
# ---------------------------

# Parse command line
project_name = parse_args_or_exit()

# Parse ini file
types = parse_types_ini_or_exit(project_name)

# Process all files replacing types
process_files(project_name, types)
